api = 2
core = 8.x


projects[blog][version] = 2.x-dev
projects[blog][type] = module
projects[blog][subdir] = contrib

projects[apigee_edge][version] = 1.0-beta1
projects[apigee_edge][type] = module
projects[apigee_edge][subdir] = contrib